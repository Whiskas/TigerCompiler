:- reconsult(lex).
:- reconsult(syn).
%:- reconsult('sem.prolog').

cc(File) :-
    compile(File, Code),
    %write(Code),
    nl.

compile(File, Code) :-
    readFile(File, In), (printSteps -> write(In), nl; true),
    lex(Tokens, In, []), (printSteps -> write(Tokens), nl; true),
    syn(Tree, Tokens, []), (printSteps -> write(Tree), nl; true).%,

%    sem(SymTab, Tree),
%    codeGen(Tree, SymTab, IntCode),
%    mdd(IntCode, Code).

readFile(File, ASCIICodes) :-
    open(File, read, Stream),
    streamToCode(Stream, ASCIICodes).

streamToCode(Stream, Codes) :-
    get_code(Stream, Code),
    (Code = -1 ->
	 Codes = [end_of_file],
	 close(Stream);
     Codes = [Code|MG],
     streamToCode(Stream, MG)).

print :- assert(printSteps).
noprint :- retract(printSteps).
