
lex([]) --> [end_of_file].
lex(R) --> token(ign(_)), lex(R).
lex([Token|R]) --> token(Token),lex(R).

token(T) --> toperator(T).
token(T) --> tkeyword(T).
token(id(T)) --> tid(T).
token(n(T)) --> tnatural(T).
token(ign(T)) --> tign(T).
token(str(T)) --> tstr(T).

%toperator(lpar,["("|B],B). %o mm que
toperator(lpar) --> "(".
toperator(rpar) --> ")".
toperator(lbrack) --> "[".
toperator(rbrack) --> "]".
toperator(lbrace) --> "{".
toperator(rbrace) --> "}".

toperator(assign) --> ":=".
toperator(colon) --> ":".
toperator(dot) --> ".".
toperator(comma) --> ",".
toperator(semicolon) --> ";".

toperator(asterisk) --> "*".
toperator(slash) --> "/".
toperator(plus) --> "+".
toperator(minus) --> "-".

toperator(equal) --> "=".
toperator(diferent) --> "<>".
toperator(less) --> "<".
toperator(greater) --> ">".
toperator(le) --> "<=".
toperator(ge) --> ">=".
toperator(amp) --> "&".
toperator(pipe) --> "|".

tkeyword(if) --> "if".
tkeyword(then)     --> "then".
tkeyword(else)     --> "else".
tkeyword(array)    --> "array".
tkeyword(break)    --> "break".
tkeyword(do)       --> "do".
tkeyword(end)      --> "end".
tkeyword(for)      --> "for".
tkeyword(function) --> "function".
tkeyword(in)       --> "in".
tkeyword(let)      --> "let".
tkeyword(nil)      --> "nil".
tkeyword(of)       --> "of".
tkeyword(to)       --> "to".
tkeyword(type)     --> "type".
tkeyword(var)      --> "var".
tkeyword(while)    --> "while".
tkeyword(new)      --> "new".
tkeyword(import)   --> "import".
tkeyword(primitive) --> "primitive".
tkeyword(class)    --> "class".
tkeyword(method)   --> "method".
tkeyword(extends)  --> "extends".

tid(S) --> letra(L),
          alfas(L1),
          {junta(L,L1,S)}.

letra(a) --> "a".
letra(b) --> "b".
letra(c) --> "c".
letra(d) --> "d".
letra(e) --> "e".
letra(f) --> "f".
letra(g) --> "g".
letra(h) --> "h".
letra(i) --> "i".
letra(j) --> "j".
letra(k) --> "k".
letra(l) --> "l".
letra(m) --> "m".
letra(n) --> "n".
letra(o) --> "o".
letra(p) --> "p".
letra(q) --> "q".
letra(r) --> "r".
letra(s) --> "s".
letra(t) --> "t".
letra(u) --> "u".
letra(v) --> "v".
letra(w) --> "w".
letra('x') --> "x".
letra(y) --> "y".
letra(z) --> "z".

letra('A') --> "A".
letra('B') --> "B".
letra('C') --> "C".
letra('D') --> "D".
letra('E') --> "E".
letra('F') --> "F".
letra('G') --> "G".
letra('H') --> "H".
letra('I') --> "I".
letra('J') --> "J".
letra('K') --> "K".
letra('L') --> "L".
letra('M') --> "M".
letra('N') --> "N".
letra('O') --> "O".
letra('P') --> "P".
letra('Q') --> "Q".
letra('R') --> "R".
letra('S') --> "S".
letra('T') --> "T".
letra('U') --> "U".
letra('V') --> "V".
letra('W') --> "W".
letra('X') --> "X".
letra('Y') --> "Y".
letra('Z') --> "Z".


alfas(A) --> letra(L), alfas(A1), {junta(L, A1, A)}.
alfas(A) --> digit(D), alfas(A1), {junta(D, A1, A)}.
alfas('') --> [].

%tnatural(N,A,Z) :- digit(D,A,B), natural(D,N,B,Z). %regra de baixo convertida para esta
tnatural(N) --> digit(D), natural(D,N).
natural(A,N) --> digit(D), { A1 is A*10 + D }, natural(A1,N).
natural(N,N) --> [].

digit(0) --> "0".
digit(1) --> "1".
digit(2) --> "2".
digit(3) --> "3".
digit(4) --> "4".
digit(5) --> "5".
digit(6) --> "6".
digit(7) --> "7".
digit(8) --> "8".
digit(9) --> "9".

tign(newline) --> "\n".
tign(newline) --> "\r".
tign(newline) --> "\n\r".
tign(newline) --> "\r\n".
tign(blankspace) --> " ".
tign(end_of_file) --> [end_of_file].
%tign comments

tstr(A) --> [34],alfas(A),[34].


%%%Funções auxiliares%%%
append([],L,L).
append([X|L],LR,[X|LN]) :-
    append(L, LR, LN).

junta(A, B, C) :-
    atomic_list_concat([A, B], C).
