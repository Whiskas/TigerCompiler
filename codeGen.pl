%codeGen(ArvSint, Code3End,)
codeGen(Var, [n(X)], n(X),[], Var).
codeGen([var(id(I),Exp)|Rest],[C,atrib(id(I),Temp)|Rest2]) :-
    codeGen([Exp],[C|Rest0]),
    Rest0 = [],
    arg(1,C,Temp),
    codeGen(Rest,Rest2).
codeGen([var(id(I),Exp)|Rest],[C,Rest0,atrib(id(I),Temp)|Rest2]) :-
    codeGen([Exp],[C|Rest0]),
    Rest0 \= [],
    arg(1,C,Temp),
    codeGen(Rest,Rest2).

%codeGen/5
%codeGen(NumVar, ArvSint, Var, Code3End, NumVar2)
codeGen(NVar,[expAri(sum,Exp1,Exp2)|Rest],Var, Cod3End, NVar4) :-
    codeGen(NVar, [Exp2],Var2, E2,NVar1),
    codeGen(NVar1,[Exp1],Var1, E1,NVar2),
    var(NVar2,NVar3),
    codeGen(NVar3, Rest, Var3, Rest2, NVar4),
    append(E1,E2,E0),
    append(E0,[atrib(NVar2,Var1,sum,Var2)|Rest2],Cod3End),
    atomic_concat('t',NVar2, Var).

codeGen([expAri(minus,Exp1,Exp2)|Rest],[atrib(t,Exp1,minus,Exp2)|Rest2]) :- codeGen(Rest,Rest2).
codeGen([expAri(asterisk,Exp1,Exp2)|Rest],[atrib(t,Exp1,asterisk,Exp2)|Rest2]) :- codeGen(Rest,Rest2).
codeGen([expAri(slash,Exp1,Exp2)|Rest],[atrib(t,Exp1,slash,Exp2)|Rest2]) :- codeGen(Rest,Rest2).

codeGen(L,[neg(Exp1)|Rest],[atrib(t,n(0),minus,Exp1)|Rest2],L) :- codeGen(Rest,Rest2).

codeGen(NVar, [expLog(equal,Exp1,Exp2)|Rest],Var, Cod3End, NVar4) :-
    codeGen(NVar, [Exp2],Var2, E2,NVar1),
    codeGen(NVar1,[Exp1],Var1, E1,NVar2),
    var(NVar2,NVar3),
    codeGen(NVar3, Rest, Var3, Rest2, NVar4),
    append(E1,E2,E0),
    append(E0,[atrib(NVar2,Var1,equal,Var2)|Rest2],Cod3End),
    atomic_concat('t',NVar2, Var).

codeGen([expLog(diferent,Exp1,Exp2)|Rest],[atrib(t,Exp1,diferent,Exp2)|Rest2]) :- codeGen(Rest,Rest2).
codeGen([expLog(greater,Exp1,Exp2)|Rest],[atrib(t,Exp1,greater,Exp2)|Rest2]) :- codeGen(Rest,Rest2).
codeGen([expLog(less,Exp1,Exp2)|Rest],[atrib(t,Exp1,less,Exp2)|Rest2]) :- codeGen(Rest,Rest2).
codeGen([expLog(ge,Exp1,Exp2)|Rest],[atrib(t,Exp1,ge,Exp2)|Rest2]) :- codeGen(Rest,Rest2).
codeGen([expLog(le,Exp1,Exp2)|Rest],[atrib(t,Exp1,le,Exp2)|Rest2]) :- codeGen(Rest,Rest2).

codeGen(NVar,[or(Exp1,Exp2)|Rest],Var, Cod3End, NVar4) :-
    codeGen(NVar, [Exp2],Var2, E2,NVar1),
    codeGen(NVar1,[Exp1],Var1, E1,NVar2),
    var(NVar2,NVar3),
    codeGen(NVar3, Rest, Var3, Rest2, NVar4),
    append(E1,E2,E0),
    append(E0,[atrib(NVar2,Var1,pipe,Var2)|Rest2],Cod3End),
    atomic_concat('t',NVar2, Var).

codeGen([and(Exp1,Exp2)|Rest],[atrib(t,Exp1,amp,Exp2)|Rest2]) :- codeGen(Rest,Rest2).

codeGen(NVar, [],NVar, [], NVar).

%codeelse([],).
%codeelse(Exp,(atrib(t,Exp),label(l))
codeGen(NVar, [ifthenelse(Exp1,Exp2,Exp3)|Rest],Varl, E6, NVar5) :-
    codeGen(NVar, [Exp2],Var2, E2,NVar1),
    codeGen(NVar1,[Exp1],Var1, E1,NVar2),
    codeGen(NVar2, Exp3,Var3, E3,NVar3),
    var(NVar3,NVar4),
    codeGen(NVar4, Rest, Var4, Rest2, NVar5),
    atomic_concat('t',NVar, Var0),
    append(E1, [if_false(Var1,l1)],E0),
    append(E0,E2,E4),
    append(E4,[l1],E5),
    append(E5,E3,E6),
    atomic_concat('t',NVar4, Var).


codeGen([while(Exp1,Exp2)|Rest],[atrib(t1,Exp1),label(L1),if_false(t1,L2),atrib(t2,Exp2),jump(L1),label(L2)|Rest2],L2) :- 
	  label(L,L1),label(L1,L2),codeGen(Rest,Rest2).
codeGen([for(id(I),Exp1,Exp2,Exp3)|Rest],[id(I),atrib(I,Exp1),label(L1),atrib(t1,Exp2),if_false(t1,L2),atrib(t2,Exp3),jump(L1),label(L2)|Rest2],L2) :- 
	  label(L,L1),label(L1,L2),codeGen(Rest,Rest2).


label(L,L1) :-
    L1 is L+1.

var(NVar, NVar1) :-
    NVar1 is NVar+1.

append([],L,L).
append([X|L],LR,[X|LN]) :-
    append(L, LR, LN).
