%syn(Tree,A,B) :- program(Tree,A,B).
syn(Tree) --> program(Tree).

program(Exp) --> exp(Exp).

%Variables, fields, elements of an array
exp(LValue) --> lvalue(LValue).
lvalue(var(I)) --> [id(I)].
lvalue(field(I,Value)) --> [id(I), dot], lvalue(Value).
lvalue(elem_array(I,Exp)) --> [id(I),lbrack], exp(Exp), [rbrack].
lvalue(elem_array(I,Value,Exp)) --> [id(I), dot], lvalue(Value), [lbrack], exp(Exp), [rbrack].

%Operations
exp6(neg(Exp)) --> [minus], exp4(Exp). %
exp(Exp) --> exp2(Exp).
exp(or(Exp1,Exp2)) --> exp2(Exp1), [pipe], exp(Exp2).
exp2(Exp) --> exp3(Exp).
exp2(and(Exp1,Exp2)) --> exp3(Exp1), [amp], exp2(Exp2).
exp3(Exp) --> exp4(Exp).
exp3(expLog(Op,Exp1,Exp2)) --> exp4(Exp1), op_comp(Op), exp4(Exp2).
exp4(Exp) --> exp5(Exp).
exp4(expAri(Op,Exp1,Exp2)) --> exp5(Exp1), op_ari(Op), exp4(Exp2).
exp5(Exp) --> exp6(Exp).
exp5(expAri(Op,Exp1,Exp2)) --> exp6(Exp1), op_ari2(Op), exp5(Exp2).
exp6(nil) --> [nil].
exp6(n(N)) --> [n(N)].
exp6(LValue) --> lvalue(LValue).
exp6(Exps) --> [lpar], exp_seq(Exps), [rpar].
op_ari2(mult) --> [asterisk].
op_ari2(div) --> [slash].
op_ari(sum) --> [plus].
op_ari(sub) --> [minus].
op_comp(equal) --> [equal].
op_comp(diff) --> [diferent].
op_comp(greater) --> [greater].
op_comp(less) --> [less].
op_comp(ge) --> [ge].
op_comp(le) --> [le].

exp_seq(Exp) --> exp(Exp).
exp_seq(exps(Exp,Exps)) --> exp(Exp), [semicolon], exp_seq(Exps).

%Assignment
exp(atrib(LValue,Exp)) --> lvalue(LValue), [assign], exp.

%Function call
exp(fcall(I)) -->
    [id(I), lpar, rpar].
exp(fcall(I,Exps)) -->
    [id(I), lpar], exp_list(Exps) , [rpar].
exp_list(Exp) --> exp(Exp).
exp_list([Exp|Exps]) --> exp(Exp),[comma], exp_list(Exps).

%% %Array creation.
exp(array_creat(Type,Exp1,Exp2)) --> type_id(Type), [lbrack], exp(Exp1), [rbrack], [of], exp(Exp2).

%% %Record creation.
exp(rec_creat(Type, Field)) --> type_id(Type), [lbrace], field_create(Field),[rbrace].
field_create(field(id(I),Exp)) --> [id(I),equal],exp(Exp).
field_create(fields(field(id(I),Exp),Field)) --> [id(I),equal], exp(Exp),[comma], field_create(Field).

%Object creation.
exp(obj_creat(Type)) --> [new], type_id(Type).

exp(let(Decs,Exps)) --> [let], decs(Decs) , [in], exp(Exps), [end].
exp(ifthenelse(Exp1, Exp2, Exp3)) --> [if], exp(Exp1), [then], exp(Exp2), else(Exp3).
else(Exp) --> [].
else(Exp) --> [else], exp(Exp).
exp(for(id(I),Exp1,Exp2, Exp3)) --> [for, id(I),assign], exp(Exp1), [to], exp(Exp2), [do], exp(Exp3).
exp(while(Exp1,Exp2)) --> [while], exp(Exp1), [do], exp(Exp2).
exp(mcall(L,I,Exps)) --> lvalue(L), [dot, id(I), lpar] , exp_list(Exps), [rpar].
exp(break) --> [break].

decs(Dec) --> dec(Dec).
decs([Dec|Decs]) --> dec(Dec), decs(Decs).

dec(dec(id(I),Type)) --> [type, id(I), equal] , ty(Type).
dec(class(id(I),Type,ClassF)) --> [class], [id(I)], [extends], type_id(Type), [lbrace], classfields(ClassF), [rbrace].
dec(Var) --> vardec(Var).
dec(Fun) --> fundec(Fun).
dec(primitive(id(I),TypeFields,Type)) --> [primitive, id(I)], [lpar] , tyfields(TypeFields), [rpar] ,primitive(Type).
primitive([]) --> [].
primitive(Type) --> [colon], type_id(Type).
dec(import(str(I))) --> [import, str(I)].

vardec(var(id(I), Exp1)) --> [var], [id(I)], [assign], exp(Exp1).
vardec(var(id(I),Type,Exp1)) --> [var], [id(I)], [colon] , type_id(Type), [assign], exp(Exp1).

fundec(function(id(I),Fields,Exp1)) --> [function], [id(I)], [lpar], tyfields(Fields), [rpar], [equal], exp(Exp1).
fundec(function(id(I),Fields,Type,Exp1)) --> [function], [id(I)], [lpar], tyfields(Fields), [rpar],[colon],type_id(Type),[equal], exp(Exp1).


classDec([]) --> [].
classDec(Fields) --> classfields(Fields).
classfields([Field]) --> classfield(Field).
classfields([Field|Fields]) --> classfield(Field), [comma], classfields(Fields).
classfield(Var) --> vardec(Var).
classfield(method(id(I),Fields,Type,Exp1)) --> [method], [id(I)], [lpar], tyfields(Fields), [rpar], [colon], type_id(Type), [equal], exp(Exp1).

ty(type(I)) --> type_id(I).
ty(rec(Fields)) --> [lbrace], fieldDec(Fields), [rbrace].
ty(array(Type)) --> [array], [of], type_id(Type).
ty(class(ty(ExtendType), Classfields)) --> [class] , ty2(ExtendType), [lbrace] , classfields(Classfields), [rbrace].
ty2([]) --> [].
ty2(Type) --> [extends], type_id(Type).

fieldDec([]) --> [].
fieldDec(Fields) --> tyfields(Fields).
tyfields([]) --> [].
tyfields(Fields) --> tyfields2(Fields).
tyfields2([field(id(I),Type)]) --> [id(I)], [colon], type_id(Type).
tyfields([field(id(I),Type) | Fields]) --> [id(I)], [colon], type_id(Type),[comma], tyfields(Fields).


type_id(I) --> [id(I)].
